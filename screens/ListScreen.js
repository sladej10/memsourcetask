import React, { Component } from 'react';
import {
    View,
    ActivityIndicator,
    ScrollView,
    ListView,
    Text,
    StyleSheet,
    Slider,
    Picker,
    NetInfo,
    TouchableHighlight,
    Image,
    BackHandler,
    RefreshControl,
} from 'react-native';
import styles from 'res/styles';
import colors from 'res/colors';
import strings from 'res/strings';
import constants from 'res/constants';
import RequestHelper from 'RequestHelper';
import Storage from 'Storage';
import Util from 'Util';
import Project from 'Project';

const DUE_IN_HOURS_OPTIONS = [4, 8, 24, 72];

const FetchStatus = {
    NOT_FETCHED: 1,
    SUCCESS: 2,
    ERROR_NO_CONNECTION: 3,
    ERROR_OTHER: 4,
}

class ListScreen extends Component {

    static navigationOptions = (navigation) => ({
        title: 'Projects list',
        headerLeft: null,
    })

    constructor(props) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2,
            sectionHeaderHasChanged: (s1, s2) => s1 !== s2
        });
        this.state = {
            token: null,
            expires: null,
            dataSource: ds,
            dueInHoursOptionIndex: DUE_IN_HOURS_OPTIONS.length - 1,
            fetchStatus: FetchStatus.NOT_FETCHED,
            allProjects: [],
            filteredProjects: [],
        };
    }

    androidBackHandler() {
        BackHandler.exitApp();
        return true;
    }

    componentDidMount() {
        var self = this;
        Storage.getToken((token) => {
            self.setState({ token });
            Storage.getExpires((expires) => {
                self.setState({ expires });
                var token = self.state.token;
                var expires = self.state.expires;
                if (token && expires && Util.isTokenValid(token, expires)) {
                    self.fetchProjects();
                } else {
                    self.navigateToLogin();
                    self.setState({
                        token: null,
                        expires: null,
                    });
                }
            })
        });
        BackHandler.addEventListener('hardwareBackPress', this.androidBackHandler.bind(this));
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress');
    }

    navigateToLogin() {
        const { navigate } = this.props.navigation;
        navigate('Login', {});
    }

    handleFetchError(responseJson) {
        if (responseJson.errorCode === constants.API_AUTH_UNAUTHORIZED) {
            // Go back to login
            this.navigateToLogin();
        }
        this.setState({ fetchStatus: FetchStatus.ERROR_OTHER, refreshing: false });
    }

    handleFetchSuccessful(responseJson) {
        projects = this.parseProjects(responseJson);
        projects.sort((a, b) => {
            return a.getDueInHours() - b.getDueInHours();
        })

        this.setState({
            fetchStatus: FetchStatus.SUCCESS,
            allProjects: projects,
            refreshing: false,
        }, () => this.filterProjects(this.state.dueInHoursOptionIndex))
            ;
    }

    httpPost(url, body, callback) {
        return new Promise((resolve, reject) => {
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function () {
                if (xmlHttp.readyState == 4) {
                    if (xmlHttp.status == 200) {
                        resolve(xmlHttp.responseText);
                    } else {
                        reject(FetchStatus.ERROR_OTHER);
                    }
                }
            }
            xmlHttp.open("POST", url, true);
            xmlHttp.setRequestHeader("Content-type", "application/json");
            xmlHttp.timeout = 30000;
            xmlHttp.onTimeout = () => reject(FetchStatus.ERROR_NO_CONNECTION);
            //xmlHttp.setRequestHeader("Content-length", body.length);
            //xmlHttp.setRequestHeader("Connection", "close");
            xmlHttp.send(body);
        });
    }

    onRefresh() {
        this.setState({
            projects: [],
            filteredProjects: []
        });
        this.fetchProjects();
    }

    fetchProjects() {
        PROJECTS_URL = `${constants.BASE_URL}/project/list?token=${this.state.token}`;
        dueInHours = DUE_IN_HOURS_OPTIONS[DUE_IN_HOURS_OPTIONS.length - 1];
        body = JSON.stringify({
            dueInHours: dueInHours,
        });

        // Fetch seems to sometimes break on Android since 3/2016 (https://github.com/facebook/react-native/issues/6679)
        //, so we use old good XmlHttpRequest
        this.httpPost(PROJECTS_URL, body).then((responseText) => {
            if (!responseText) {
                this.handleFetchError.bind(this)(responseJson);
                return;
            }
            var responseJson = JSON.parse(responseText);
            if (responseJson.errorCode) {
                this.handleFetchError.bind(this)(responseJson);
            } else {
                this.handleFetchSuccessful.bind(this)(responseJson.projects);
            }
        }).catch((error) => {
            if (error == FetchStatus.ERROR_NO_CONNECTION || error == FetchStatus.ERROR_OTHER) {
                console.log('Fetch error: request: ', error);
                this.setState({ fetchStatus: error, refreshing: false });
            } else {
                console.log('Fetch error: unknown: ', error);
                this.setState({ fetchStatus: FetchStatus.ERROR_OTHER, refreshing: false });
            }
        })

        /*fetch(PROJECTS_URL, RequestHelper.postRequestOptions(body))
            .then((response) => response.json())
            .then((responseJson) => {
                console.log('fetchProjects response: ', responseJson);
                if (responseJson.errorCode) {
                    this.handleFetchError(responseJson);
                } else {
                    //console.log('Projects type: ', rJson.projects);
                    this.handleFetchSuccessful(responseJson.projects);
                }
                return null;
            }).catch((e) => {
                console.log('FetchProjects error', responseJson);
            }).done(() => {
                console.log('FetchProjects done')
            });*/
    }

    parseProjects(projectsJson) {
        result = [];
        for (var proj in projectsJson) {
            if (!projectsJson.hasOwnProperty(proj)) {
                continue;
            }
            var project = projectsJson[String(proj)];
            p = new Project();
            p.name = project.name;
            p.sourceLang = project.sourceLang;
            p.targetLangs = project.targetLangs;
            p.status = project.status;
            p.dateDue = Util.fixDateStringTimezone(project.dateDue);
            result.push(p);
        }
        return result;
    }

    

    handleDueInHoursChange(optionIndex) {
        this.setState({
            dueInHoursOptionIndex: optionIndex
        })
        this.filterProjects(optionIndex);
    }

    filterProjects(optionIndex) {
        var result = this.state.allProjects.filter((p) => {
            return p.getDueInHours() < DUE_IN_HOURS_OPTIONS[optionIndex];
        })
        this.setState({
            filteredProjects: result,
            dataSource: this.state.dataSource.cloneWithRows(result),
        });
    }

    tryAgain() {
        this.setState({ fetchStatus: FetchStatus.NOT_FETCHED });
        this.fetchProjects();
    }

    openDetail(rowData, rowId) {
        const { navigate } = this.props.navigation;
        navigate('Details', { project: rowData });
    }

    renderLoadingView() {
        return (
            <View style={styles.container}>
                <ActivityIndicator
                    color={colors.ACCENT}
                    size='large'
                    animating={true}
                />
            </View>
        );
    }

    renderErrorView() {
        var text = strings.UNABLE_TO_FETCH_PROJECTS;
        if (this.state.fetchStatus === FetchStatus.ERROR_NO_CONNECTION) {
            text = strings.NO_CONNECTION;
        }
        return <View style={styles.container}>
            <Text style={styles.label}>{text}</Text>
            <TouchableHighlight style={styles.buttonContainer}
                onPress={this.tryAgain.bind(this)}>
                <Text style={styles.buttonText}>{strings.TRY_AGAIN}</Text>
            </TouchableHighlight>
            {
                this.renderFooter()
            }
        </View >
    }

    logout() {
        Storage.setToken('')
            .then(() => Storage.setExpires(''))
            .then(() => {
                const { navigate } = this.props.navigation;
                navigate('Login');
            });
    }

    renderListWithControls() {
        let pickerItems = DUE_IN_HOURS_OPTIONS.map((v, i) => {
            return <Picker.Item style={{ fontSize: 12 }} key={i} label={strings.X_HOURS(v)} value={v} />
        });
        return <ScrollView
            contentContainerStyle={[styles.container, { padding: 40, flexDirection: 'column', alignItems: 'flex-start' }]}
        >
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 15, marginLeft: 2}}>
                <Text style={[styles.smallLabel, { flexGrow: 1, textAlign: 'right' }]}>{strings.SHOWING_PROJECTS_DUE_IN_AT_MOST}</Text>
                <Picker
                    style={[styles.picker, { flexGrow: 1 }]}
                    itemStyle={styles.pickerItem}
                    prompt={strings.SELECT_NUMBER_OF_HOURS}
                    selectedValue={DUE_IN_HOURS_OPTIONS[this.state.dueInHoursOptionIndex]}
                    onValueChange={(itemValue, itemIndex) => this.handleDueInHoursChange(itemIndex)}>
                    {pickerItems}
                </Picker>
            </View>
            <Slider
                style={styles.slider}
                step={100 / (DUE_IN_HOURS_OPTIONS.length - 1)}
                value={(100 / (DUE_IN_HOURS_OPTIONS.length - 1)) * this.state.dueInHoursOptionIndex}
                thumbTintColor={colors.ACCENT}
                minimumValue={0}
                maximumValue={100}
                enabled={this.state.allProjects.length > 0}
                onValueChange={value => {
                    this.handleDueInHoursChange(Util.findNearestOptionIndex(value, DUE_IN_HOURS_OPTIONS))
                }
                }
            />
            {this.renderListViewOrEmpty()}
            {this.renderFooter()}
        </ScrollView>
    }
    //contentContainerStyle={styles.listStyle}

    renderFooter() {
        return (
            <TouchableHighlight
                style={styles.buttonContainer}
                onPress={this.logout.bind(this)}>
                <Text style={styles.buttonText}>
                    {strings.LOG_OUT}
                </Text>
            </TouchableHighlight>
        );
    }

    renderListViewOrEmpty() {
        return (
            <View
                style={styles.listWrapperStyle}>
                <ListView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this.onRefresh.bind(this)}
                        />
                    }
                    enableEmptySections={true}
                    style={styles.listStyle}
                    dataSource={this.state.dataSource}
                    renderRow={(rowData, rowId) => rowData.renderForList(() => this.openDetail(rowData, rowId))}
                />
                {
                    this.state.filteredProjects.length == 0 ? (
                        < Text style={styles.label} > {strings.NO_MATCHING_PROJECTS_FOUND}</Text>
                    ) : (<View />)
                }
            </View>
        )

    }

    resolveContentView() {
        switch (this.state.fetchStatus) {
            case FetchStatus.NOT_FETCHED:
                return this.renderLoadingView();
            case FetchStatus.ERROR_OTHER:
            case FetchStatus.ERROR_NO_CONNECTION:
                return this.renderErrorView();
            case FetchStatus.SUCCESS:
                return this.renderListWithControls();
            default:
                return <View style={styles.container} />;
        }
    }

    render() {
        //const {navigate} = this.props.navigation;
        return this.resolveContentView();
    }
}
export default ListScreen;

