import React, { Component } from 'react';
import {
  StyleSheet,
  AsyncStorage,
  AppRegistry,
  ActivityIndicator,
  Text,
  TextInput,
  View,
  NetInfo,
  TouchableWithoutFeedback,
  ListView,
  TouchableHighlight,
  Dimensions,
  KeyboardAvoidingView,
} from 'react-native';
import colors from 'res/colors';
import strings from 'res/strings';
import styles from 'res/styles';
import constants from 'res/constants';
import RequestHelper from 'RequestHelper';
import ListScreen from 'screens/ListScreen';
import Storage from 'Storage';
import Util from 'Util';
import ProjectsNavigator from 'ProjectsNavigator';

const LoginError = {
  NO_CONNECTION: 1,
  BAD_CREDENTIALS: 2,
  UNKNOWN: 3
}

class LoginScreen extends Component {

  static navigationOptions = {
    title: '',
    //headerMode: 'none',
    //header: null,
    headerLeft: null,
  }

  constructor(props) {
    super(props);
    this.state = ({
      token: null,
      expires: null,
      triedLoadToken: false,
      username: '',
      password: '',
    });
  }

  componentDidMount() {
    var self = this;
    Storage.getToken((token) => {
      Storage.getExpires((expires) => {
        self.setState({ token, expires });
        if (self.state.token && self.state.expires && Util.isTokenValid(self.state.token, self.state.expires)) {
          self.navigateToList();
        } else {
          self.setState({
            triedLoadToken: true,
          });
        }
      })
    });
  }


  handleSuccessfulLogin(token, expiresDate) {
    var expires = new Date(expiresDate).getTime() / 1000;

    //TODO remove
    //token = 'KQKJb02OZSyAUxyMwk2WROxhAlaA0N0AWeUmDMmAH4msFqKFgUGVsMGtOOK1CnJZc';
    //expires = '1514640644';
    //TODO remove end

    Storage.setToken(token)
      .then(() => Storage.setExpires(expires))
      .then(() => {
        // Change state here so that next screen has token in storage ready
        this.setState({
          token: token,
          expires: expires,
          loginError: null,
          isLoggingIn: false,
        })
        this.navigateToList();
      }
      );
  }

  login = () => {
    this.setState({
      token: null,
      isLoggingIn: true,
    });
    var username = encodeURIComponent(this.state.username);
    var password = encodeURIComponent(this.state.password);
    const LOGIN_URL = constants.BASE_URL + `/auth/login?userName=${username}&password=${password}`;

    // Check connection
    NetInfo.isConnected.fetch()
      .then(isConnected => {
        if (!isConnected) {
          this.loginFailedWithError(LoginError.NO_CONNECTION);
          console.log('Login failed, no connection');
          return;
        }
        // Send login request
        fetch(LOGIN_URL, RequestHelper.getRequestOptions())
          .then(response => response.json())
          .then(responseJson => {
            if (responseJson.token && responseJson.expires) {
              // Expiration date's timezone is not parseable for JS, fix it
              var fixedExpireDate = Util.fixDateStringTimezone(responseJson.expires);
              if (!fixedExpireDate) {
                this.loginFailedWithError(LoginError.UNKNOWN_ERROR);
                console.log('Login failed, could not parse token expiration');
                return;
              }
              // Login successful
              this.handleSuccessfulLogin(responseJson.token, fixedExpireDate)
            } else if (responseJson.errorCode === constants.API_AUTH_INVALID_CREDENTIALS) {
              // Invalid credentials
              console.log('Bad credentials');
              this.loginFailedWithError(LoginError.BAD_CREDENTIALS);
            } else {
              // Unknown error
              console.log('Unknown error');
              this.loginFailedWithError(LoginError.UNKNOWN);
            }
          })
          .catch((error) => {
            // Connection failed
            this.loginFailedWithError(LoginError.NO_CONNECTION);
            console.error('Login failed: ', error);
          })

      })
      .catch(error => {
        // Connection test failed
        this.loginFailedWithError(LoginError.NO_CONNECTION);
      })
  }

  loginFailedWithError(error) {
    this.setState({
      loginError: error,
      isLoggingIn: false
    }
    );
  }

  navigateToList() {
    const { navigate } = this.props.navigation;
    navigate('List');
  }

  renderLoginView() {
    var loginErrorText = '';
    switch (this.state.loginError) {
      case LoginError.BAD_CREDENTIALS:
        loginErrorText = strings.BAD_CREDENTIALS;
        break;
      case LoginError.NO_CONNECTION:
        loginErrorText = strings.NO_CONNECTION;
        break;
      case LoginError.UNKNOWN:
        loginErrorText = strings.UNKNOWN_ERROR;
        break;
    }
    return (
      <KeyboardAvoidingView
        style={styles.container}
        behavior='padding'>
        <Text style={styles.titleLabel}>
          {strings.APP_NAME}
        </Text>
        <TextInput
          style={styles.input}
          placeholder='username'
          autoCorrect={false}
          autoCapitalize='none'
          value={this.state.username}
          onChangeText={(username) => this.setState({ username })}
          onSubmitEditing={() => this.refs.passwordInput.focus()}
        />
        <TextInput
          ref='passwordInput'
          style={styles.input}
          secureTextEntry={true}
          placeholder='password'
          value={this.state.password}
          autoCorrect={false}
          autoCapitalize='none'
          onChangeText={(password) => this.setState({ password })}
          onSubmitEditing={this.login}
        />
        <Text style={[styles.label, styles.alert]}>
          {loginErrorText}
        </Text>
        <TouchableHighlight
          style={styles.buttonContainer}
          disabled={!this.state.username || !this.state.password}
          onPress={this.login}>
          <Text style={styles.buttonText}>
            {strings.LOG_IN}
          </Text>
        </TouchableHighlight>
      </KeyboardAvoidingView>
    )
  }

  renderLoadingView() {
    return (
      <View style={styles.container}>
        <ActivityIndicator
          color={colors.ACCENT}
          size='large'
          animating={true}
        />
      </View>
    );
  }

  render() {
    if (!this.state.triedLoadToken || this.state.isLoggingIn) {
      return this.renderLoadingView();
    }
    if (!this.state.token || !Util.isTokenValid(this.state.token, this.state.expires)) {
      return this.renderLoginView();
    }
    return <View style={styles.container}><Text>...</Text></View>
  }
}
export default LoginScreen;