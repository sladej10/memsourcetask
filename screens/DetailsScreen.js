import React, { Component } from 'react';
import {
    View,
    Text,
    BackHandler,
} from 'react-native';
import styles from 'res/styles';

class DetailsScreen extends Component {

    static navigationOptions = {
        title: 'Project details',
    }

    constructor(props) {
        super(props);
        this.state = {
            project: null
        }
    }

    androidBackHandler() {
        const { navigate } = this.props.navigation;
        navigate('List')
        return true;
    }

    componentDidMount() {
        if (this.props.navigation.state.params) {
            this.setState({
                project: this.props.navigation.state.params.project,
            })
        } else {
            const { navigate } = this.props.navigation;
            navigate('List');
        }
        BackHandler.addEventListener('hardwareBackPress', this.androidBackHandler.bind(this));

    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress');
    }

    render() {
        if (this.state.project) {
            return (
                this.state.project.renderForDetails()
            )
        } else {
            return <View style={styles.container}><Text>Nothing to show</Text></View>;
        }
    }

}
export default DetailsScreen;

