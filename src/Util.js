export default class Util {

    static setStateSafe(ref, state) {
        if(this.refs[ref]) {
            this.setState(state);
        }
    }

    static findNearestOptionIndex(percentage, options) {
        // Find nearest value
        var optionCount = options.length;
        var left = 0, right = optionCount - 1;
        var pivot = Math.floor((left + right) / 2);
        var found = false;
        while (left <= right) {
            var p = pivot * (100 / (optionCount - 1));
            if (percentage > p) {
                left = pivot + 1;
            } else if (percentage < p) {
                right = pivot - 1;
            } else {
                found = true;
                break;
            }
            pivot = Math.floor((left + right) / 2);
        }
        if (!found) {
            var leftVal = left * (100 / (optionCount - 1));
            var rightVal = right * (100 / (optionCount - 1));
            pivot = Math.abs(percentage - leftVal) >
                Math.abs(percentage - rightVal) ? right : left;
        }
        return pivot;
    }

    static fixDateStringTimezone(input) {
        // Add colon between timezone hours and minutes
        var regex = /^(.*)(\+)([0-9]{2})([0-9]{2}$)/;
        var chunks = input.match(regex);
        if (chunks && chunks.length >= 5) {
            return chunks[1] + chunks[2] + chunks[3] + ':' + chunks[4];
        } else {
            return null;
        }
    }

    static isTokenValid(token, expires) {
        var expiresDate;
        if(!expires) {
            return false;
        }
        var now = new Date().getTime() / 1000;
        isValid = expires > now;
        return isValid;
    }

}