import React from 'react';
import {
    AsyncStorage
} from 'react-native';
import Util from 'Util';
const KEY_USER_TOKEN_EXPIRES = '@MemsourceTask:user_token_expires';
const KEY_USER_TOKEN = '@MemsourceTask:user_token';

export default class Storage {

    static async getToken(callback) {
        var token = await AsyncStorage.getItem(KEY_USER_TOKEN);
        callback(token);
    }

    static async getExpires(callback) {
        var expires = await AsyncStorage.getItem(KEY_USER_TOKEN_EXPIRES);
        callback(expires);
    }

    static async setToken(token) {
        await AsyncStorage.setItem(KEY_USER_TOKEN, token);
    }

    static async setExpires(expires) {
        await AsyncStorage.setItem(KEY_USER_TOKEN_EXPIRES, String(expires));
    }

}