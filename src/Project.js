import React from 'react';
import globalStyles from 'res/styles';
import colors from 'res/colors';
import strings from 'res/strings';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';

export default class Project {
    name = null;
    sourceLang = null;
    targetLangs = null;
    status = null;
    dateDue = null;

    getFormattedTargetLangs(langs) {
        const length = langs.length;
        var result = '';
        for(var i = 0; i < langs.length; i++) {
            result += langs[i];
            if(i < langs.length - 1) {
                result += ', ';
            }
        }
        return result;
    }

    renderForList(rowClickCallback) {
        return (

            <TouchableOpacity style={listStyles.rowContainer}
                onPress={rowClickCallback}
            >
                <View style={listStyles.rowInnerContainer}>
                    <Text style={listStyles.rowTitle}>{this.name}</Text>
                    <Text style={listStyles.rowSubtitle}>
                        {this.sourceLang + ' -> ' + this.getFormattedTargetLangs(this.targetLangs)}
                    </Text>
                </View>
                <View style={listStyles.rowInnerContainer}>
                    <Text style={listStyles.rowInfo}>{strings.DUE_IN_X_HOURS(Math.floor(this.getDueInHours()))}</Text>
                    <Text style={[listStyles.rowInfo, {color:colors.ACCENT}]}>{this.status}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    renderForDetails() {
        return (

            <View style={detailStyles.container}>
                <Text style={detailStyles.title}>{this.name}</Text>
                <Text style={detailStyles.subtitle}>
                    {strings.LANGUAGES + ': ' + this.sourceLang + ' -> ' + this.getFormattedTargetLangs(this.targetLangs)}
                </Text>
                <Text style={detailStyles.info}>{strings.DUE_IN_X_HOURS(Math.floor(this.getDueInHours()))}</Text>
                <Text style={[detailStyles.info, {color:colors.ACCENT}]}>{this.status}</Text>

            </View>
        )
    }

    getDueInHours() {
        var now = new Date().getTime();
        var due = new Date(this.dateDue).getTime();
        return Math.max(0, (due - now) / (1000 * 3600));
    }

};

const detailStyles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
    },
    title: {
        color: colors.ACCENT,
        fontSize: 25,
        textAlign: 'center',
        padding: 5,
    },
    subtitle: {
        color: colors.TEXT,
        fontSize: 15,
        textAlign: 'center',
        padding: 5,
    },
    info: {
        color: colors.TEXT,
        fontSize: 13,
        textAlign: 'center',
        padding: 3,
    }
});

const listStyles = StyleSheet.create({
    rowContainer: {
        flex: 1,
        flexDirection: 'row',
        padding: 12,
    },
    rowInnerContainer: {
        flex: 1,
        flexDirection: 'column',
    },
    rowTitle: {
        flex: 4,
        color: colors.TEXT,
        textAlign: 'left',
        fontSize: 20,
        marginBottom: 3,
    },
    rowInfo: {
        flex: 1,
        textAlign: 'right',
        color: colors.TEXT_SECONDARY,
        fontSize: 12
    },
    rowSubtitle: {
        color: colors.TEXT_SECONDARY,
        fontSize: 14
    },

});