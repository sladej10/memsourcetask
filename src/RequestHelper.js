
const BASE_REQUEST_OPTIONS = {
  timeout: 3000,
  headers: {
    'User-Agent': 'test-app-x'
  }
}

class RequestHelper {

  static getRequestOptions() {
    return Object.assign(BASE_REQUEST_OPTIONS,
      {
        method: 'GET'
      });
  }

  static postRequestOptions(body) {
    return Object.assign(BASE_REQUEST_OPTIONS,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body
      });
  }

}
export default RequestHelper;