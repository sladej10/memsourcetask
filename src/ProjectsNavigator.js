
import {
  BackHandler
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import ListScreen from 'screens/ListScreen';
import LoginScreen from 'screens/LoginScreen';
import DetailsScreen from 'screens/DetailsScreen';
import styles from 'res/styles';
import colors from 'res/colors';
import constants from 'res/constants';


const baseNavigationOptions = (navigation) => ({
  headerStyle: styles.headerStyle,
  headerTintColor: colors.TEXT_INVERSE,
  headerMode: 'float',
});

const ProjectsNavigator = StackNavigator({
  Login: {
    screen: LoginScreen,
    title: 'Login',
    navigationOptions: Object.assign(baseNavigationOptions, 
      LoginScreen.navigationOptions)
    
  },
  List: {
    screen: ListScreen,
    title: 'Projects list',
    navigationOptions: Object.assign(baseNavigationOptions, 
      ListScreen.navigationOptions)
  },
  Details: {
    screen: DetailsScreen,
    title: 'Project details',
    navigationOptions: baseNavigationOptions
  }
});

export default ProjectsNavigator;