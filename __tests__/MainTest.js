import 'react-native';
import React from 'react';
import Util from '../src/Util';
import renderer from 'react-test-renderer';

const DUE_IN_HOURS_OPTIONS = [4, 8, 24, 72];

it('Finding nearest option index works', () => {
    expect(Util.findNearestOptionIndex(5, DUE_IN_HOURS_OPTIONS)).toBe(0);
    expect(Util.findNearestOptionIndex(38, DUE_IN_HOURS_OPTIONS)).toBe(1);
    expect(Util.findNearestOptionIndex(69, DUE_IN_HOURS_OPTIONS)).toBe(2);
    expect(Util.findNearestOptionIndex(96, DUE_IN_HOURS_OPTIONS)).toBe(3);
});