const strings = {
    APP_NAME : 'Memsource Project Browser',
    BAD_CREDENTIALS : 'Invalid username or password',
    NO_CONNECTION : 'No connection',
    UNKNOWN_ERROR : 'Unknown error',
    TRY_AGAIN : 'Try again',
    LANGUAGES : 'Languages',
    LOG_IN : 'Log in',
    UNABLE_TO_FETCH_PROJECTS : 'Unable to fetch projects',
    LOG_OUT : 'Log out',
    SELECT_MAXIMUM_HOURS_BEFORE_DUE_DATE: 'Select maximum hours before due date',
    NO_PROJECTS_MATCH_GIVEN_CRITERIA : 'No projects match given criteria',
    X_HOURS : (hours) => `${hours} hours`,
    DUE_IN_X_HOURS : (hours) => `due in ${strings.X_HOURS(hours)}`,
    SHOWING_PROJECTS_DUE_IN_AT_MOST : 'showing projects due within',
}

export default strings;