const constants = {
    BASE_URL : 'https://cloud.memsource.com/web/api/v4',
    API_AUTH_INVALID_CREDENTIALS : 'AuthInvalidCredentials',
    STATUS_BAR_HEIGHT : 24
};
export default constants;