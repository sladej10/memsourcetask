const colors = {
    TEXT_SELECTED : '#888888',
    TEXT : '#666666',
    TEXT_SECONDARY : '#999999',
    TEXT_INVERSE : '#FFFFFF',
    ACCENT : '#0FA0CA',
    ALERT : '#DF2200',
}
export default colors;
