import {
    StyleSheet, Platform, StatusBar
} from 'react-native';
import colors from 'res/colors'; 

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
      paddingVertical: 12,
      paddingHorizontal: 12,
    },
    slider: {
      alignSelf: 'stretch',
      marginTop: 12,
      marginBottom:12,
    },
    picker: {
      alignSelf: 'stretch',
      width: 120,
    },
    pickerItem: {
      padding: 8,
      textAlign: 'center',
      color: colors.ACCENT,
      fontSize: 12,
    },
    input: {
      fontSize: 18,
      textAlign: 'center',
      color: colors.TEXT,
      alignSelf: 'stretch',
      paddingVertical: 12,
    },
    label: {
      fontSize: 18,
      textAlign: 'center',
      margin: 5,
      alignSelf: 'stretch',
    },
    smallLabel: {
      fontSize: 16,
      textAlign: 'center',
    },
    alert: {
      color: colors.ALERT,
    },
    headerStyle: {
      marginTop: (Platform.OS === 'ios' ? 20 : StatusBar.currentHeight),
      backgroundColor: colors.ACCENT,
    },
    listWrapperStyle: {
      flex: 1,
      flexDirection: 'row',
      alignSelf: 'stretch',
    },
    listStyle: {
      flexGrow: 1,
      flexDirection: 'column',
      alignSelf: 'stretch',
    },
    titleLabel: {
      color: colors.ACCENT,
      fontSize: 22,
      fontWeight: 'bold',
      textAlign: 'center',
      margin: 5,
      alignSelf: 'stretch',
    },
    buttonContainer: {
      backgroundColor: colors.ACCENT,
      alignSelf: 'stretch',
      justifyContent: 'center',
      paddingVertical: 15,
      marginTop: 5,
    },
    buttonText: {
      color: '#FFFFFF',
      textAlign: 'center'
    },
  });
  export default styles;