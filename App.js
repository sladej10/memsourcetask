import React, { Component } from 'react';
import {
    View,
    Text,
    Dimensions,
    StatusBar
} from 'react-native';
import ProjectsNavigator from 'ProjectsNavigator';
import styles from 'res/styles';

export default class App extends Component {

    render() {
        return (
                <ProjectsNavigator />
        )
    }

}